﻿using System.Collections;
using System.Collections.Generic;
using FoodItems;
using UnityEngine;

[CreateAssetMenu(fileName = "FoodContainer", menuName = "ScriptableObjects/FoodContainerSO", order = 1)]
public class FoodContainerSO : ScriptableObject
{
    public Food[] Foods;
}
