﻿using System;
using System.Linq;
using FoodItems;
using UnityEngine;

namespace ObjectFactories.FoodFactory
{
    /// <summary>
    /// Instantiate wanted food from Food Container Scriptable object, prefabs in container must have related component on it.
    /// </summary>
    public class FoodFactory : GenericFactory<Food>
    {
        #region Singleton
        public static FoodFactory Instance { get; private set; }
        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }
        #endregion
        
        public FoodContainerSO foodContainer;
        
        /// <summary>
        /// Instantiate wanted food from Food Container Scriptable object, prefab must have component on it.
        /// </summary>
        /// <param name="foodType"></param>
        public Food GetNewFood(FoodType foodType)
        {
            Food food = null;
            switch (foodType)
            {
                case FoodType.Banana:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Banana>());
                    break;
                case FoodType.Cheese:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Cheese>());
                    break;
                case FoodType.Olive:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Olive>());
                    break;
                case FoodType.Cherry:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Cherry>());
                    break;
                case FoodType.HotDog:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<HotDog>());
                    break;
                case FoodType.Hamburger:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Hamburger>());
                    break;
                case FoodType.Watermelon:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Watermelon>());
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(foodType), foodType, "Can not find object in food container.");
            }
            Food newGO = null;
            
            if (food)
                newGO = GetNewInstance(food);
            
            return newGO;
        }
        /// <summary>
        /// Instantiate wanted food from Food Container Scriptable object, prefab must have component on it.
        /// </summary>
        /// <param name="foodType"></param>
        /// <param name="spawnPosition"></param>

        public Food GetNewFood(FoodType foodType,Vector3 spawnPosition)
        {
            Food food = null;
            switch (foodType)
            {
                case FoodType.Banana:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Banana>());
                    break;
                case FoodType.Cheese:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Cheese>());
                    break;
                case FoodType.Olive:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Olive>());
                    break;
                case FoodType.Cherry:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Cherry>());
                    break;
                case FoodType.HotDog:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<HotDog>());
                    break;
                case FoodType.Hamburger:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Hamburger>());
                    break;
                case FoodType.Watermelon:
                    food = foodContainer.Foods.First(f => f.gameObject.GetComponent<Watermelon>());
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(foodType), foodType, "Can not find object in food container.");
            }
            Food newGO = null;
            
            if (food)
                newGO = GetNewInstance(food,spawnPosition);
            
            return newGO;
        }
    }
}
