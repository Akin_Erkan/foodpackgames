﻿using UnityEngine;

namespace ObjectFactories
{
    public class GenericFactory<T> : MonoBehaviour where T : MonoBehaviour
    {
        /// <summary>
        /// Creating new instance of wanted MonoBehaviour object.
        /// </summary>
        /// <returns>New instance of MonoBehaviour object.</returns>
        protected T GetNewInstance(T t)
        {
            return Instantiate(t);
        }
        /// <summary>
        /// Creating new instance of wanted MonoBehaviour object with given position.
        /// </summary>
        /// <returns>New instance of MonoBehaviour object.</returns>
        protected T GetNewInstance(T t,Vector3  position)
        {
            return Instantiate(t,position,Quaternion.identity);
        }
    }
}
