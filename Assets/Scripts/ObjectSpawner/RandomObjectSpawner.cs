﻿using System.Collections;
using FoodItems;
using ObjectFactories.FoodFactory;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomObjectSpawner : MonoBehaviour
{
    public Transform spawnPoint;
    public int objectCount = 10;
    public float circleRadius;
    public float spawnRadius;

    /// <summary>
    /// Spawn given food type at random position according to this class instance variables.
    /// </summary>
    /// <param name="foodType"></param>
    /// <returns></returns>
    public IEnumerator SpawnObjectsAtRandomPosition(FoodType foodType)
    {
        for (int i = 0; i < objectCount; i++)
        {
            var foodGO = FoodFactory.Instance.GetNewFood(foodType);
            var rndPos = GenerateRandomPos(spawnPoint, circleRadius);
            foodGO.transform.position = rndPos;
            foodGO.name = i.ToString();
            yield return null;
        }  
    }
    
    /// <summary>
    /// Spawn given food type at random position without colliding each other, according to this class instance variables.
    /// </summary>
    /// <param name="foodType"></param>
    /// <returns></returns>
    public IEnumerator SpawnObjectsAtRandomPointWithoutCollision(FoodType foodType)
    {
        for (int i = 0; i < objectCount; i++)
        {
            var foodGO = FoodFactory.Instance.GetNewFood(foodType);
            var rndPos = GenerateRandomPos(spawnPoint, circleRadius);
            while(Physics.CheckSphere(rndPos, spawnRadius)) //check position till detect no collision in given radius.
            {
                rndPos = GenerateRandomPos(spawnPoint, circleRadius); // getting new position if collision detected in radius
                yield return null;
            }
            foodGO.transform.position = rndPos;
            foodGO.name = i.ToString();
        }
    }
    
    public static Vector3 GenerateRandomPos(Transform referencePoint, float radius) => referencePoint.position + Random.insideUnitSphere * radius;
}
