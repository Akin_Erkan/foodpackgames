﻿namespace FoodItems
{
    public enum FoodType
    {
        Olive,
        Cheese,
        Cherry,
        Banana,
        HotDog,
        Hamburger,
        Watermelon
    }
    public interface IFood
    {
    }
}
